# Slim profile for production like environment
FROM python:3.9-slim

# Working Directory
WORKDIR /code

# User (non-root) setup
RUN groupadd -r fastapi && useradd -ms /bin/bash -r -g fastapi fastapi
ENV PATH /home/fastapi/.local/bin:${PATH}
USER fastapi

# Application setup
COPY ./app /code/app
RUN /usr/local/bin/python -m pip --disable-pip-version-check install --upgrade pip
RUN pip install -U --no-cache-dir --upgrade -r /code/app/requirements.txt

# Container Run Command. Not using entrypoint so that it can be easily overwritten.
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8080"]
