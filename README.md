# FastAPI Testing

## Purpose

Testing out FastAPI to get a good feel for it, figuring out best practices/testing, and adding some useful tooling for FastAPI based projects.

One of the main items is the cli which I'd like to use to replace a lot of the random commands you have to run to maintain the project.

By random commands, I mean like the [useful commands](#useful-commands) section below.
I would rather run something like `fastapi testsuite --full` than to have to type everything out over and over each time. 

The CLI will eventually be part of a seperate project, but for now I'm including it so that I can develop it while I'm working with FastAPI, similar to TDD, but with libs and CLIs.

## Project Setup

1. `python3 -m venv venv`
2. `. venv/bin/activate`
3. `python -m pip install --upgrade pip`
4. `pip install -r requirements.txt`

## Useful Commands

### App Testing

- Formatting: `black app`
- Testing: `pytest --cov app --cov-report html app-tests`
  - For checking coverage, open the `index.html` file in a browser.
  - In VSCode, you can right click on `index.html` and run using the live server.
    - This runs the server on `http://localhost:5500/htmlcov/index.html`
- Security: `bandit -r app/`
- Linting: `pylint app`

### App Run

- Docker Compose Setup: `docker-compose up --build`
- Docker Compose Cleanup: `docker-compose down --rmi local`

### CLI Testing

- TODO

### CLI Run

- TODO

## TODO

### CLI

- [ ] CLI Setup
- [ ] CLI Tests
- [ ] CLI Packaging/Compiling
- [ ] CLI Installation

### Database

- [ ] Database Connection
- [ ] Research Migrations
- [ ] Database Setup Using Migrations
- [ ] Database Monkeypatch Testing

### IAM

- [ ] Authentication
- [ ] Authorization
- [ ] OAuth/OIDC

### Caching

- [ ] Caching

### Job Runners

- [ ] Async Jobs

### Advanced Queries and Search

- [ ] Advanced Queries/Search

### User Interface

- [ ] Simple UI
