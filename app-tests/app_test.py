from fastapi.testclient import TestClient
from app.main import app

client = TestClient(app)


def test_read_main():
    response = client.get("/")

    assert response.status_code == 200
    assert response.json() == {"msg": "Hello World"}


def test_health_check():
    response = client.get("/healthcheck")

    assert response.status_code == 200
    assert response.json() == {"status": "healthy"}


def test_list_anime():
    response = client.get("/anime")

    assert response.status_code == 200
    assert response.json() == [
        {
            "stub": "haruhi-suzumiya-no-yuutsu",
            "name": "Haruhi Suzumiya no Yuutsu",
            "episodes": 14,
            "still_airing": False,
        }
    ]


def test_get_specific_anime():
    response = client.get("/anime/haruhi-suzumiya-no-yuutsu")

    assert response.status_code == 200
    assert response.json() == {
        "stub": "haruhi-suzumiya-no-yuutsu",
        "name": "Haruhi Suzumiya no Yuutsu",
        "episodes": 14,
        "still_airing": False,
    }


def test_fail_retrieving_non_existent_anime():
    response = client.get("/anime/dragon-ball-z")

    assert response.status_code == 404
    assert response.json()["detail"] == "Anime not found"


def test_adding_anime_to_database():
    data = {"name": "Sayonara Zetsubou-Sensei", "episodes": 12, "still_airing": False}

    response = client.put("/anime", json=data)

    assert response.status_code == 201
    assert response.json() == {
        "stub": "sayonara-zetsubou-sensei",
        "name": "Sayonara Zetsubou-Sensei",
        "episodes": 12,
        "still_airing": False,
    }


def test_anime_already_in_database():
    data = {"name": "Sayonara Zetsubou-Sensei", "episodes": 12, "still_airing": False}

    response = client.put("/anime", json=data)

    assert response.status_code == 400
    assert response.json()["detail"] == "Anime already in Database"
