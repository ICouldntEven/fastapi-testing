"""
FastAPI Testing - Main Setup File

Disabled some pylint due to python version issues with pydantic
"""

from string import punctuation
from fastapi import FastAPI, status, HTTPException
from pydantic import BaseModel  # pylint: disable=E0611
from loguru import logger

app = FastAPI()


class Anime(BaseModel):  # pylint: disable=R0903

    """Anime Model for database entry"""

    stub: str
    name: str
    episodes: int
    still_airing: bool


class AnimeInput(BaseModel):  # pylint: disable=R0903

    """Input Validation Model for requests"""

    name: str
    episodes: int
    still_airing: bool


psuedo_db = [
    {
        "stub": "haruhi-suzumiya-no-yuutsu",
        "name": "Haruhi Suzumiya no Yuutsu",
        "episodes": 14,
        "still_airing": False,
    }
]


def anime_in_db(stub: str) -> bool:

    """Acting like stubs are primary keys"""

    logger.debug(f"Validating if {stub} is in DB.")
    db_search = [anime for anime in psuedo_db if anime["stub"] == stub]
    logger.debug(f"Search Results: {db_search}")
    return bool(db_search)


def generate_anime_name_stub(name: str) -> str:

    """Generates a stub for the Anime BaseModel"""

    logger.debug(f"Generating Stub for {name}.")
    name_lower = name.lower()
    name_lower_special_chars_removed = name_lower.replace(punctuation, " ")
    anime_name_stub = "-".join(name_lower_special_chars_removed.split(" "))
    logger.debug(f"Generated Stub: {anime_name_stub}")
    return anime_name_stub


@app.get("/")
async def root():

    """Root endpoint - Used for testing"""

    logger.debug("Root View Called")
    return {"msg": "Hello World"}


@app.get("/healthcheck")
async def health_check():

    """Health check for the service."""

    logger.debug("Health at max!")
    return {"status": "healthy"}


@app.get("/anime")
async def anime_list():

    """Returns a list of anime"""

    logger.debug("Returning Anime Table")
    return psuedo_db


@app.get("/anime/{stub}")
async def anime_detail(stub: str):

    """Returns a specific anime from the DB"""

    logger.debug(f"Selecting {stub} from DB")
    selected_anime = [anime for anime in psuedo_db if anime["stub"] == stub]

    if len(selected_anime) != 1:
        logger.warning(f"{stub} not found.")
        raise HTTPException(status_code=404, detail="Anime not found")

    return selected_anime[0]


@app.put("/anime", status_code=status.HTTP_201_CREATED)
async def add_anime(anime_input: AnimeInput):

    """Adds an anime to the DB"""

    logger.debug(f"AnimeInput: {anime_input}")

    stub = generate_anime_name_stub(anime_input.name)
    logger.debug(f"Generated Stub {stub}")

    if anime_in_db(stub):
        logger.warning(f"{stub} already in Database")
        raise HTTPException(status_code=400, detail="Anime already in Database")

    mapped_input = anime_input.dict()
    logger.debug(f"Mapped Input: {mapped_input}")

    anime = Anime(**mapped_input, stub=stub)
    logger.debug(f"Adding {stub} to DB")

    psuedo_db.append(anime.dict())
    logger.debug(f"Added Anime {anime}")

    return anime
